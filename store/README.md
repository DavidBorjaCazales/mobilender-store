Mobilender Store  API

## Installation steps without Docker

1. Ensure you have python3 installed

2. Clone the repository
3. create a virtual environment using `virtualenv venv`
4. Activate the virtual environment by running `source venv/bin/activate`

- On Windows use `source venv\Scripts\activate`

5. Install the dependencies using `pip install -r requirements.txt`

6. Migrate existing db tables by running `python manage.py migrate`

7. Run the django development server using `python manage.py runserver`


## Installation steps for Docker

tUses the default Django development server.


Update the environment variables in the docker-compose.yml and .env files.

Build the images and run the containers:

$ docker-compose up -d --build
Test it out at http://localhost:8000. The "app" folder is mounted into the container and your code changes apply automatically.