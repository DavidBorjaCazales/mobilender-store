from django.db import models

# Create your models here.

class Client_Type(models.Model):
	"""
	Model Client_Type
	"""
	name = models.CharField(max_length=100)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@staticmethod
	def get_client_types():
		return Client_Type.objects.all()

	def __str__(self):
		return f"{self.name}_{self.code}"

class Client(models.Model):
	"""
	Model Client
	"""
	client_type_id = models.ForeignKey(Client_Type, on_delete=models.CASCADE)
	name = models.CharField(max_length=100)
	code = models.CharField(max_length=100)
	url_photo = models.CharField(max_length=100)
	address = models.CharField(max_length=100)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


	@staticmethod
	def get_clients():
		return Client.objects.all()

	def __str__(self):
		return f"{self.name}_{self.code}"