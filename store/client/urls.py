from django.urls import path
from . import views


urlpatterns = [
    path('', views.ClientAPIView.as_view(), name="client"),
    path('/<int:pk>', views.ClientDetail.as_view(), name="client_detail"),

    path('/client_type', views.ClientTypeAPIView.as_view(), name="client_type"),
    path('/client_type/<int:pk>', views.ClientTypeDetail.as_view(), name="client_type_detail")
]
