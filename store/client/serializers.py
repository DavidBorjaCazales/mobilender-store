from rest_framework import serializers
from .models import Client_Type, Client


class ClientTypeSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client_Type
        fields = ['id', 'name']


class ClientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = [
        	'id', 'client_type_id',
        	'name', 'code',
        	'url_photo', 'address'
        ]
