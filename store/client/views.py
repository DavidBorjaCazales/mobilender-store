from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import Client_Type, Client
from .serializers import ClientTypeSerializer, ClientSerializer

# Create your views here.

class ClientTypeAPIView(ListCreateAPIView):
	serializer_class = ClientTypeSerializer
	queryset = Client_Type.get_client_types()


class ClientTypeDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = ClientTypeSerializer
	queryset = Client_Type.get_client_types()


class ClientAPIView(ListCreateAPIView):
	serializer_class = ClientSerializer
	queryset = Client.get_clients()


class ClientDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = ClientSerializer
	queryset = Client.get_clients()