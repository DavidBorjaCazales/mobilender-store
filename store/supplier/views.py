from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import Supplier
from .serializers import SupplierSerializer

# Create your views here.

class SupplierAPIView(ListCreateAPIView):
	serializer_class = SupplierSerializer
	queryset = Supplier.get_suppliers()


class SupplierDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = SupplierSerializer
	queryset = Supplier.get_suppliers()