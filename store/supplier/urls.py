from django.urls import path
from . import views


urlpatterns = [
    path('', views.SupplierAPIView.as_view(), name="supplier"),
    path('/<int:pk>', views.SupplierDetail.as_view(), name="supplier_detail")
]
