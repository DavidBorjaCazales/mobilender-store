from django.db import models

# Create your models here.

class Supplier(models.Model):
	"""
	Model Client
	"""
	name = models.CharField(max_length=100)
	address = models.CharField(max_length=200)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@staticmethod
	def get_suppliers():
		return Supplier.objects.all()

	def __str__(self):
		return f"{self.name}"