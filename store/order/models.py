from django.db import models
from client.models import Client
from article.models import Article

# Create your models here.

class OrderBranch(models.Model):
	"""
	Model OrderBranch
	"""
	reference = models.CharField(max_length=100)
	code_branch = models.CharField(max_length=100)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@staticmethod
	def get_order_branches():
		return OrderBranch.objects.all()

	@staticmethod
	def get_order_branch(pk):
		order = OrderBranch.objects.filter(
		    pk=pk
		).first()
		return order

	def __str__(self):
		return f"{self.code_branch}"


class AssociatedCompanyOrder(models.Model):
	"""
	Model AssociatedCompanyOrder
	"""
	reference = models.CharField(max_length=100)
	code_parther = models.CharField(max_length=100)
	order_detail = models.CharField(max_length=300)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@staticmethod
	def get_associated_company_orders():
		return AssociatedCompanyOrder.objects.all()

	@staticmethod
	def get_associated_company_order(pk):
		order = AssociatedCompanyOrder.objects.filter(
		    pk=pk
		).first()
		return order

	def __str__(self):
		return f"{self.reference}"


class OrderDistributionCenter(models.Model):
	"""
	Model OrderDistributionCenter
	"""
	stock = models.CharField(max_length=300)

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)

	@staticmethod
	def get_order_distribution_centers():
		return OrderDistributionCenter.objects.all()

	@staticmethod
	def get_order_distribution_center(pk):
		order = OrderDistributionCenter.objects.filter(
		    pk=type_order_id
		).first()
		return order

	def __str__(self):
		return f"{self.id}"


class Order(models.Model):
	"""
	Model Order
	"""
	client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
	article_id = models.ForeignKey(Article, on_delete=models.CASCADE)
	# ForeignKey for order types:  OrderDistributionCenter
	# AssociatedCompanyOrder & OrderBranch

	type_order_id = models.IntegerField()

	order_type = models.CharField(max_length=30)
	quantity = models.IntegerField()

	is_urgent = models.BooleanField(default=False)

	order_at = models.DateTimeField(auto_now=True)
	stock_at = models.DateTimeField()

	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


	@staticmethod
	def get_orders():
		return Order.objects.all()

	@staticmethod
	def get_order(pk):
		return Order.objects.all()


	@staticmethod
	def get_order_by_parameters(data):
		# here goes the logic of the query
		return Order.objects.all()

	def __str__(self):
		return f"{self.id}"
