from django.urls import path
from . import views


urlpatterns = [

	
	path('/associated_company_orders_urgents', views.OrderDetailByParameters.as_view(), name='order_distribution_center_urgent'),

    path('', views.OrderAPIView.as_view(), name="order"),
    path('/<int:pk>', views.OrderDetail.as_view(), name="order_detail"),

    path('/order_branch', views.OrderBranchAPIView.as_view(), name="order_branch"),
    path('/order_branch/<int:pk>', views.OrderBranchDetail.as_view(), name="order_branch_detail"),

    path('/associated_company_order', views.AssociatedCompanyOrderAPIView.as_view(), name="associated_company_order"),
    path('/associated_company_order/<int:pk>', views.AssociatedCompanyOrderDetail.as_view(), name="associated_company_order_detail"),

    path('/order_distribution_center', views.OrderDistributionCenterAPIView.as_view(), name="order_distribution_center"),
    path('/order_distribution_center/<int:pk>', views.OrderDistributionCenterDetail.as_view(), name="order_distribution_center_detail")
]
