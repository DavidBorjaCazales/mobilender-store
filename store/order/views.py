from rest_framework import status
from rest_framework.response import Response
from rest_framework.renderers import JSONRenderer
from django.shortcuts import render
from rest_framework.generics import (
	ListCreateAPIView,
	RetrieveUpdateDestroyAPIView,
	GenericAPIView
)
from .models import (
	Order,
	OrderBranch,
	AssociatedCompanyOrder,
	OrderDistributionCenter
)
from .serializers import (
	OrderSerializer,
	OrderBranchSerializer,
	AssociatedCompanyOrderSerializer,
	OrderDistributionCenterSerializer
)

# Create your views here.

class OrderBranchAPIView(ListCreateAPIView):
	serializer_class = OrderBranchSerializer
	queryset = OrderBranch.get_order_branches()


class OrderBranchDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = OrderBranchSerializer
	queryset = OrderBranch.get_order_branches()


class AssociatedCompanyOrderAPIView(ListCreateAPIView):
	serializer_class = AssociatedCompanyOrderSerializer
	queryset = AssociatedCompanyOrder.get_associated_company_orders()


class AssociatedCompanyOrderDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = AssociatedCompanyOrderSerializer
	queryset = AssociatedCompanyOrder.get_associated_company_orders()


class OrderDistributionCenterAPIView(ListCreateAPIView):
	serializer_class = OrderDistributionCenterSerializer
	queryset = OrderDistributionCenter.get_order_distribution_centers()


class OrderDistributionCenterDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = OrderDistributionCenterSerializer
	queryset = OrderDistributionCenter.get_order_distribution_centers()


class OrderAPIView(ListCreateAPIView):
	serializer_class = OrderSerializer
	queryset = Order.get_orders()


class OrderDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = OrderSerializer
	queryset = Order.get_orders()


class OrderDetailByParameters(GenericAPIView):

	def get(self, request):
		orders = Order.get_order_by_parameters(
			{
				"client_type": "silver",
				"is_urgent": True,
				"tipo_pedido": "OrderDistributionCenter",
				"is_assortment": True

			}
		)

		order_serializer = OrderSerializer(orders).data
		content = JSONRenderer().render(order_serializer)

		return Response(content, status=status.HTTP_200_OK)

