from rest_framework import serializers
from .models import (
    Order,
	OrderBranch,
	AssociatedCompanyOrder,
	OrderDistributionCenter
)


class OrderBranchSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderBranch
        fields = ['id', 'reference', 'code_branch']


class AssociatedCompanyOrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = AssociatedCompanyOrder
        fields = ['id', 'reference', 'reference', 'order_detail']


class OrderDistributionCenterSerializer(serializers.ModelSerializer):

    class Meta:
        model = OrderDistributionCenter
        fields = ['id', 'stock']


class OrderSerializer(serializers.ModelSerializer):

    class Meta:
        model = Order
        fields = [
            'id', 'client_id', 
            'article_id', 'type_order_id', 
            'type_order_id', 'order_type',
            'quantity', 'is_urgent',
            'order_at', 'stock_at'
        ]


    def validate(self, attrs):
        type_order_id = attrs.get('type_order_id')
        order_type = attrs.get('order_type')
        quantity = attrs.get('quantity')

        if order_type == "order_branch":
            order = OrderBranch.get_order_branch(
                type_order_id
            )

            if order is None:
                raise serializers.ValidationError(
                    'Invalid order_branch id'
                )

        elif order_type == "associated_company_order":
            order = AssociatedCompanyOrder.get_associated_company_order(
                type_order_id
            )

            if order is None:
                raise serializers.ValidationError(
                    'Invalid associated_company_order id'
                )

        elif order_type == "order_distribution_center":
            order = OrderDistributionCenter.get_order_distribution_center(
                type_order_id
            )

            if order is None:
                raise serializers.ValidationError(
                    'Invalid order_distribution_center id'
                )
        else:
            raise serializers.ValidationError(
                'Invalid order_type'
            )

        if quantity < 1:
            raise serializers.ValidationError(
                'quantity  order must be greater than 0'
            )
        return attrs

