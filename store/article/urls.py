from django.urls import path
from . import views


urlpatterns = [
    path('', views.ArticleAPIView.as_view(), name="article"),
    path('/<int:pk>', views.ArticleDetail.as_view(), name="article_detail")
]
