from django.shortcuts import render
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from .models import Article
from .serializers import ArticleSerializer

# Create your views here.

class ArticleAPIView(ListCreateAPIView):
	serializer_class = ArticleSerializer
	queryset = Article.get_articles()


class ArticleDetail(RetrieveUpdateDestroyAPIView):
	serializer_class = ArticleSerializer
	queryset = Article.get_articles()