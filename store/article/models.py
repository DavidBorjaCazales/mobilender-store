from django.db import models
from supplier.models import Supplier
# Create your models here.


class Article(models.Model):
	"""
	Model Article
	"""
	supplier_id = models.ForeignKey(Supplier, on_delete=models.CASCADE)
	code = models.CharField(max_length=100)
	description = models.CharField(max_length=200)
	precio = models.FloatField()
	created_at = models.DateTimeField(auto_now_add=True)
	updated_at = models.DateTimeField(auto_now=True)


	@staticmethod
	def get_articles():
		return Article.objects.all()

	def __str__(self):
		return f"{self.id}_{self.code}"